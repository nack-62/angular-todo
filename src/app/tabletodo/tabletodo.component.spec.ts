import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabletodoComponent } from './tabletodo.component';

describe('TabletodoComponent', () => {
  let component: TabletodoComponent;
  let fixture: ComponentFixture<TabletodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabletodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabletodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
