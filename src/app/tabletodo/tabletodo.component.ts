import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { TodosService } from './../services/todos.service';
import { Todo } from 'src/app/model/todo.model';

@Component({
  selector: 'app-tabletodo',
  templateUrl: './tabletodo.component.html',
  styleUrls: ['./tabletodo.component.scss']
})
export class TabletodoComponent implements OnInit {
  filterCurrent='';
  dataSource = new MatTableDataSource<Todo>();
  displayedColumns = ['checked', 'text', 'complete', 'delete'];
  selection = new SelectionModel<Todo>(true, []);
  // selectedAll = false;
  
  @ViewChild(MatSort) sort: MatSort;
  constructor(private TodosService: TodosService) {}
  ngOnInit() {
    this.resultData('all');
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  resultData(filter: string){
    this.filterCurrent = filter;
    this.TodosService.getDataTodo(filter).subscribe(
      data => {
        this.dataSource = new MatTableDataSource<Todo>(data);
        this.dataSource.sort = this.sort;
        this.selection.clear();
      }
    )
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  setComplete(key: string, status: boolean){
    this.TodosService.setComplete(key,status)
  }
  setDelete(key: string){
    this.TodosService.setDelete(key)
  }

  setCompleteSelect(){
    this.selection.selected.forEach(
      row => {
          this.TodosService.setComplete(row.id,row.complete)
      }
    );
  }
  setDeleteSelect(){
    this.selection.selected.forEach(
      row => {
          this.TodosService.setDelete(row.id)
      }
    );
  }
}


