export interface Todo {
    id?: string;
    text: string;
    complete: Boolean;
    delete?: Boolean;
    createdAt?: any;
  }