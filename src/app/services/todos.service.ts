import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Todo } from 'src/app/model/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodosService {
  todoesCollection: AngularFirestoreCollection<Todo>;
  todoes: Observable<Todo[]>;
  Filter$: BehaviorSubject<string|null>;
  constructor(private afs: AngularFirestore) { }

getDataTodo(filter: string){
  
  if (filter ==='complete'){
    this.todoesCollection = this.afs.collection('todos', ref=>{
      return ref.where(filter,'==',true).orderBy('createdAt','desc');
    });
  }else if (filter ==='active'){
    this.todoesCollection = this.afs.collection('todos', ref=>{
      return ref.where('complete','==',false).orderBy('createdAt','desc')
    });
  }else{
    this.todoesCollection = this.afs.collection('todos', ref=>{
      return ref.orderBy('createdAt','desc');
    });
  }
  return this.todoes = this.todoesCollection.snapshotChanges()
  .pipe(
    map(
    change => {
      return change.map(
        a => {
          const data = a.payload.doc.data() as Todo;
          data.id = a.payload.doc.id;
          return data;
        }
      )
    }
  )
)
}

setComplete(key: string, status: Boolean){
  const batch = this.afs.firestore.batch();
  const Ref = this.afs.firestore.collection('todos');
  batch.update(Ref.doc(key),{complete: !status});
  batch.commit();
}
setDelete(key: string){
  const batch = this.afs.firestore.batch();
  const Ref = this.afs.firestore.collection('todos');
  batch.delete(Ref.doc(key));
  batch.commit();
}

}
