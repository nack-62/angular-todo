import { Component, OnInit, Output, HostListener } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSort, MatTableDataSource } from '@angular/material';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

import {ENTER} from '@angular/cdk/keycodes';
import { Todo } from 'src/app/model/todo.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  addTodo='';
  constructor(private afs: AngularFirestore) {}

  ngOnInit() {
  }
  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if ( event.key === 'Enter' && event.target['name'] === 'add-todo') {
      const batch = this.afs.firestore.batch();
      let data: Todo;
      const Ref = this.afs.firestore.collection('todos');
      const text = this.addTodo
      const createdAtN = firebase.firestore.FieldValue.serverTimestamp();
      data = {
        text: text,
        complete: false,
        createdAt: createdAtN,
      }
      batch.set(Ref.doc(),data);
      batch.commit()
      this.addTodo = '';    
    }
  }
}
